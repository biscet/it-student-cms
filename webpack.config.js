let path = require('path');
let HtmlWebpackPlugin = require('html-webpack-plugin');
let CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = (env, options) => {

  const mod = options.mode === "development";

  return {
    entry: './src/index.js',
    output: {
      path: path.resolve(__dirname, './public'),
      filename: 'main.js',
      publicPath: '/'
    },
    devServer: mod ? {
        overlay: true,
        historyApiFallback: true
      }
      : {},
    module: {
      rules: [
        {
          test: /\.(js|jsx|mjs)$/,
          loader: 'babel-loader',
          exclude: /node_modules/
        },
        {
          test: /\.css$/,
          loader: ['style-loader', 'css-loader']
        },
        {
          test: /\.scss$/,
          loaders: ['style-loader', 'css-loader',
            {
              loader: 'postcss-loader',
              options: {
                plugins: () => [require('autoprefixer')({
                  'browsers': ['ie >= 8', 'last 4 version']
                })],
              }
            },
            'sass-loader']
        },
        {
          test: /\.(jpe?g|png|gif|woff|woff2|eot|ttf|svg)$/i,
          use: mod ? 'url-loader'
            : [
              'url-loader?limit=10000&name=./static/media/[name].[hash:8].[ext]',
              {
                loader: 'img-loader',
                options: {
                  plugins: [
                    require('imagemin-gifsicle')({
                      interlaced: false
                    }),
                    require('imagemin-mozjpeg')({
                      progressive: true,
                      arithmetic: false
                    }),
                    require('imagemin-pngquant')({
                      floyd: 0.5,
                      speed: 2
                    }),
                    require('imagemin-svgo')({
                      plugins: [
                        {removeTitle: true},
                        {convertPathData: false}
                      ]
                    })
                  ]
                }
              }
            ]
        }
      ]
    },
    plugins: mod ? [
        new HtmlWebpackPlugin({
          template: './index.html',
          inject: 'body'
        })
      ]
      : [
        new CleanWebpackPlugin('public', {}),
        new HtmlWebpackPlugin({
          template: './index.html',
          inject: 'body'
        })
      ],

    performance: mod ? {}
      : {
        hints: false
      }
  };

};
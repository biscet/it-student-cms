import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter, Route, BrowserRouter, Router } from 'react-router-dom';
import App from './App';

ReactDOM.render(
  <BrowserRouter>
    <Route path='/' component={App}/>
  </BrowserRouter>
  ,document.getElementById('app'));
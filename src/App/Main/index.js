﻿import React, { Component } from 'react';
import Header from './Header';  

class Main extends Component{  
  render(){
    return<div className="Main">
      <Header />
    </div>
  }
}

export default Main;
import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import './style.scss';
import Main from './Main';
import {hot} from 'react-hot-loader';


const App = () => (
  <div>
    <Route exact path='/' component={Main}/>
    <Redirect to="/" />
  </div>
);

export default hot(module)(App);